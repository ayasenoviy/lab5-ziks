from Crypto.Cipher import AES as aes
import base64
import Crypto.Util.Counter as cntr
import sys,argparse
from sys import argv
from codecs import encode as enhex

def decode(ciphertext, key, iv):
    cr = cntr.new(128, initial_value=int.from_bytes(enhex(iv), 'big'))
    cipher = aes.new(enhex(key), aes.MODE_CTR, counter=cr)
    return cipher.decrypt(base64.b64decode(ciphertext)).decode('utf-8')

def encode(text, key, iv):
    cr = cntr.new(128, initial_value=int.from_bytes(enhex(iv), 'big'))
    cipher = aes.new(enhex(key), aes.MODE_CTR, counter=cr)
    return base64.b64encode(cipher.encrypt(text))

file = input("Path to file: ")
key = input("Key: ")
iv = input("IV: ")
choice = input('(d)ecrypt/(e)ncrypt?: ')

with open(file, 'rb') as f:
    text = f.read()
    if choice.startswith('d'):
         print(decode(text, key, iv))
    elif choice.startswith('e'):
         print(encode(text, key, iv))
    else:
         print('pnx')